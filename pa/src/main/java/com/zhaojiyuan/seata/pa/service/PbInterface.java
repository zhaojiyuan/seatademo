package com.zhaojiyuan.seata.pa.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(value = "pb")
public interface PbInterface {

    @PostMapping("/pb/pbTest")
    String callPb();
}
