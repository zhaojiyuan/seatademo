package com.zhaojiyuan.seata.pa.service;

import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service

public class PaService {

    @Autowired
    PbInterface pbInterface;

    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 先通过本地事务插入一条数据，然后远程调用Pb服务插入另外一条数据，然后本地抛出异常
     */
    @GlobalTransactional
    @Transactional
    public void paTest() {
        System.out.println("pa服务插入一条数据到数据库");
        jdbcTemplate.update("INSERT INTO tb_seata_test(nu_id, vc_text, dt_create_time) VALUES ('1', 'pa服务插入了一条数据', SYSDATE)");
        System.out.println("调用Pb服务插入一条数据到数据库");
        pbInterface.callPb();
    }
}
