package com.zhaojiyuan.seata.pa;

import com.zhaojiyuan.seata.pa.service.PaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("pa")
public class PaController {

    @Autowired
    PaService paService;

    @RequestMapping("paTest")
    public String paTest() {
        paService.paTest();
        return "1";
    }
}
