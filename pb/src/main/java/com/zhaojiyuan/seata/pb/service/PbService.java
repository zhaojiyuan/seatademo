package com.zhaojiyuan.seata.pb.service;

import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PbService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @GlobalTransactional
    @Transactional
    public void pbTest() {
        System.out.println("pbTest插入一条数据到数据库");
        jdbcTemplate.update("INSERT INTO tb_seata_test(nu_id, vc_text, dt_create_time) VALUES ('2', 'pb服务插入了一条数据', SYSDATE)");
    }
}
