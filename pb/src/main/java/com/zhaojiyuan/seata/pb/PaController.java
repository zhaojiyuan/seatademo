package com.zhaojiyuan.seata.pb;

import com.zhaojiyuan.seata.pb.service.PbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("pb")
public class PaController {

    @Autowired
    PbService pbService;

    @RequestMapping("pbTest")
    public String paTest() {
        pbService.pbTest();
        return "1";
    }
}
