# Seatademo

### 介绍
通过Springboot搭建一个简单的简单的Rest接口，使用Nacos作为注册中心注册和发现服务，集成Feign组件订阅服务实现远程调用。然后分别演示本地事务调用远程服务时候，如果本地事务发生异常，出现全局数据不一致的问题，然后集成Seata分布式事务框架处理方案，解决因为发生异常导致数据不一致的问题。

### 演示文档包含四个部分
#### 1. 环境介绍：[请点击](https://gitee.com/zhaojiyuan/study-notes/blob/master/%E6%96%87%E6%A1%A3/%E5%8D%9A%E6%96%87/seata%E5%88%86%E5%B8%83%E5%BC%8F%E4%BA%8B%E5%8A%A1(%E4%B8%80)%E7%8E%AF%E5%A2%83%E4%BB%8B%E7%BB%8D.md)

#### 2. 搭建一个本地事务环境：[传送门](https://gitee.com/zhaojiyuan/study-notes/blob/master/%E6%96%87%E6%A1%A3/%E5%8D%9A%E6%96%87/seata%E5%88%86%E5%B8%83%E5%BC%8F%E4%BA%8B%E5%8A%A1(%E4%BA%8C)%E6%90%AD%E5%BB%BA%E4%B8%80%E4%B8%AA%E6%9C%AC%E5%9C%B0%E4%BA%8B%E5%8A%A1%E7%8E%AF%E5%A2%83.md)

#### 3. 安装Seata服务器：[传送门](https://gitee.com/zhaojiyuan/study-notes/blob/master/%E6%96%87%E6%A1%A3/%E5%8D%9A%E6%96%87/seata%E5%88%86%E5%B8%83%E5%BC%8F%E4%BA%8B%E5%8A%A1(%E4%B8%89)%E5%AE%89%E8%A3%85seata%E6%9C%8D%E5%8A%A1%E5%99%A8.md)

#### 4. springboot集成seata分布式事务：[传送门](https://gitee.com/zhaojiyuan/study-notes/blob/master/%E6%96%87%E6%A1%A3/%E5%8D%9A%E6%96%87/seata%E5%88%86%E5%B8%83%E5%BC%8F%E4%BA%8B%E5%8A%A1(%E5%9B%9B)Springboot%E9%9B%86%E6%88%90seata%E5%88%86%E5%B8%83%E5%BC%8F%E4%BA%8B%E5%8A%A1.md)

###  注意事项（欢迎问题指正，共同学习，共同进步）
